# SimRail Community Manual

The following is currently an pre-release copy, please do not post a link to this guide until this message has been removed.

## General Information

### Signals

![List of Signals and signs](./Images/signal.png)

A guide and various printable reference sheets is maintained by Gazz292 on the forums, above is an example of one of the printouts. The most up to date version is avaliable [here.](https://forum.simrail.eu/topic/2090-guide-signs-and-signals-found-in-simrail-printable-cheat-sheets-explanations/)

Furthermore, later parts of this guide will refer to signals in groups, arranged like so:

* Signal Aspect - The specific series of lights and ordering that are used to impart information to an engineer.

* Stop Aspect - Any aspect informing an engineer not to pass.

* Restricting Aspect - Any aspect that allows a train to pass, but imparts a requirement, such as stop before passing, or a(n upcoming) speed limit.

* Proceed Aspect - Any aspect that allows passage of a train with no unusual restrictions.

* "Committed" Signal - Any signal where a change in aspect would change the aspect of the Signal the train is approaching. The signals affected by a change in aspect are usually the previous 3 to 4 signals, including auto blocks. Changes to a signal that a train is "committed" to should be taken extremely carefully, as revoking that signal becomes much harder to do so safely.

### General Radio Communications

### Train Codes

Type codes are a three letter code affixed to every train denoting its purpose and operational priority.

The first letter denotes its operations class, The second its Subtype:

&nbsp;&nbsp;&nbsp; E - European International

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C - Intercity

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; N - Intercity Overnight (Sleeper Trains)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I - Intercity Express

&nbsp;&nbsp;&nbsp; M - Interregional

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H - Hotel-on-Rails Express

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; M - Cross Border Express

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P - Express

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O - Local

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A - Local (Autobus)

&nbsp;&nbsp;&nbsp; R - Intraregional

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; M - Cross Border

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P - Express

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O - Local

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A - Connecter

&nbsp;&nbsp;&nbsp; A - Autobus

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; M - Cross Border Regional

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P - Regional Local

&nbsp;&nbsp;&nbsp; OK - Temporary Service (Charter)

&nbsp;&nbsp;&nbsp; P - Non Service Passenger

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; W - Out of Service

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C - Test Run

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; X - Maintanance

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H - Out of Service bulk passenger power move (>3 Locos)

&nbsp;&nbsp;&nbsp; T - Freight

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A - International Express

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C - International Long Distance

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; G - International Heavy

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; R - International Bulk

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B - Urgent

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; D - Long-Distance

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P - High Speed

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; N - Bulk

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; M - Bulk Goods

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; L - Flatcar

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; K - Transfer

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; T - Maintenance/Out of Service

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; S - Test

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H - Out of Service bulk freight power move (>3 Locos)

&nbsp;&nbsp;&nbsp; L - Locomotive Power Moves

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; M - Freight Maneuver Locomotives

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; W - Passenger Maneuver Locomotives

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P - Passenger Locomotives (<=3)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; T - Freight Locomotives (<=3)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Z - Maintenance Locomotives (<=3)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; S - Assisting Vehicle (Helper Locomotive)

&nbsp;&nbsp;&nbsp; Z - Maintenance

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; G - Relief Train

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; N - Comprehensive Inspection

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; X - Track Inspection

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H - Out of Service bulk maintenance power move (>3 Locos)

The final letter is a power type code:

&nbsp;&nbsp;&nbsp; P - Steam Locomotive

&nbsp;&nbsp;&nbsp; E - Electric Locomotive

&nbsp;&nbsp;&nbsp; J - Electric Multiple Unit

&nbsp;&nbsp;&nbsp; S - Diesel Locomotive

&nbsp;&nbsp;&nbsp; M - Diesel Multiple Unit

Some typecodes are impossible and not used, for example you will never see a Light Power Multiple Unit move, so LPJ or LPM will never show up as these are listed as PHJ or PHM, you will also never see a freight train with a multiple unit of any kind.

Train Priority can be generally described by a set of three rules:

* Passenger over Freight over Maintenance over Non Service

* Express over Non-express Non-Local over Local

* Longer Distance over Shorter Distance

* Everything else over Early trains

Significantly early trains generally drop to the bottom of the priority list, especially if they are passenger trains. The reverse however is NOT true. Late trains are on par with trains running on time priority wise, if late trains happen to be able to make up time all the better, but on time trains should not be delayed to help a late train catch up.

Number codes are an entirely seperate system, best described by this visual guide:

![Train Number Codes](./Images/numbers1.png)
![Region Numbers](./Images/numbers2.png)

From the region codes/numbers guide made by CN-BX-3N availiable [here.](https://forum.simrail.eu/topic/4650-en-understand-polish-railway-timetable-for-drivers-in-one-picture)

## Train Guides

### General

### Train Specific

#### ED250 'Pendolino'

#### EP07 '4E'

#### EP08

#### EN76 'ELF'

#### E6ACTad 'Dragon 2'

#### 'Traxx'

## Dispatch Guides

### General

### Katowice - Warszawa

#### Zawiercie

### Katowice - Sedziszow

## Useful Links

https://edr.simrail.fr - Automatic train and station timetables

## Acknowlegements

## Contributing

### Finding and submitting problems

### Contributing changes
